import React from 'react';
import PropTypes from 'prop-types';
import { Router } from 'react-router';

import { Provider } from 'react-redux';
import { StyleRoot } from 'radium';
import routes from './router/Router';


const Root = ({ store, history }) => (
    <Provider store={store}>
        <StyleRoot>
            <Router routes={routes(store)} history={history} />
        </StyleRoot>
    </Provider>
);

Root.propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default Root;