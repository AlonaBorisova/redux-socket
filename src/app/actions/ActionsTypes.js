/**
 *Socket actions
 */
export const GET_SOCKET_CONNECTION = 'GET_SOCKET_CONNECTION';
export const SET_SOCKET_CONNECTION = 'SET_SOCKET_CONNECTION';
export const CLOSE_SOCKET_CONNECTION = 'CLOSE_SOCKET_CONNECTION';
export const SET_SOCKET_DATA = 'SET_SOCKET_DATA';



export const TOGGLE_SIDE_BAR = 'TOGGLE_SIDE_BAR';