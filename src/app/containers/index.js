import NotFoundComponent from './NotFound';

export const NotFound = {
    path: '/*',
    component: NotFoundComponent,
};


// export const MyBets = {
//     path: '/user/bets',
//     component: MyBetsComponent,
// };

// export const BetDetail = {
//     path: '/user/bets/:bet_id',
//     component: BetDetailComponent,
// };
//
// export const Transactions = {
//     path: '/user/transactions',
//     component: TransactionsComponent,
// };
//
// export const UserAccount = (getState, dispatch, children) => ({
//     path: '/',
//     childRoutes: children,
//     onEnter: () => {
//         const token = dispatch(cookiesGet('token'));
//         const session = dispatch(cookiesGet('SESSION_ID'));
//         if (!token || !session) {
//             bec_query('home');
//         }
//     },
// });