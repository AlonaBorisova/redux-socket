import { Component } from 'react';
import _ from 'lodash';

class NotFound extends Component {
    shouldComponentUpdate(nextProps) {
        return !_.isEqual(this.props, nextProps);
    }
    render() {
        return null;
    }
}

export default NotFound;