import { combineReducers } from 'redux-immutable';
import application from './ApplicationReducer';
import routerReducer from './RouterReducer';

const appReducer = combineReducers({
    routing: routerReducer,
    application,
    // cashouts,
    // events,
});

export default appReducer;