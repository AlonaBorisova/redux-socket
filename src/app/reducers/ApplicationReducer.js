import { fromJS } from 'immutable';
import * as ActionTypes from '../actions/ActionsTypes';

const defaultState = fromJS({
    socket: null,
    sideBar: false,
});

export default function Application(state = defaultState, action = {}) {
    switch (action.type) {
        case ActionTypes.SET_SOCKET_CONNECTION:
            return state.set('socket', action.payload);
        case ActionTypes.TOGGLE_SIDE_BAR:
            return state.update('sideBar', (value) => !value);
        default:
            return state;
    }
}