import { browserHistory } from 'react-router';
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import rootReducer from '../reducers';

export default function configureStore(data, cookie) {
    const store = createStore(
        rootReducer,
        compose(
            applyMiddleware(
                routerMiddleware(browserHistory),
                // epicMiddleware,
                // cookieMiddleware(cookie),
                // cashoutMiddleware,
                // betsMiddleware,
            ),
        )
    );
    return store;
}