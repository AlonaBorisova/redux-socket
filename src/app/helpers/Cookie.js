/* eslint-disable */
class Cookie {
    // TODO: fix ESLint errors
    getCookieValue(offset) {
        let endstr = document.cookie.indexOf(";", offset);
        if (endstr == -1) endstr = document.cookie.length;
        return unescape(document.cookie.substring(offset, endstr));
    }
    get(name){
        const argument = name + "=";
        const argument_len = argument.length;
        const cookie_len = document.cookie.length;
        let i = 0;

        while (i < cookie_len) {
            var j = i + argument_len;
            if (document.cookie.substring(i, j) == argument) return this.getCookieValue(j);
            i = document.cookie.indexOf(" ", i) + 1;
            if (i === 0) break;
        }
        return null;
    }
    set(name, value, options = {}){
        const exp = new Date();
        exp.setTime(exp.getTime() + (20 * 60 * 60 * 1000)); // default 20 hours
        const expires = options.expires || exp;
        const path = options.path || '/';
        const domain = options.domain || null;
        const secure = options.secure || false;

        document.cookie = name + "=" + escape(value) +
            (!expires ? '' : ('; expires=' + expires.toGMTString())) +
            (!path ? '' : ('; path=' + path)) +
            (!domain ? '' : ('; domain=' + domain)) +
            (secure ? '; secure' : '');
    }
}
/* eslint-enable */
export default Cookie;