import React from 'react';
import { fromJS } from 'immutable';
import { render } from 'react-dom';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';
import './css/index.css';
import Root from './app/Root';
import Cookie from './app/helpers/Cookie';
import configureStore from './app/store/Store';
import registerServiceWorker from './registerServiceWorker';

const cookie = new Cookie();
const store = configureStore(fromJS({}), cookie);

const history = syncHistoryWithStore(browserHistory, store, {
    selectLocationState(state) {
        return state.get('routing').toJS();
    },
});

render(
    <Root store={store} history={history} />,
    document.getElementById('root')
);

registerServiceWorker();